- 创建 Stupid Assistant，并指定其管理某一个 Cabinet
- Stupid Assistant 存包，并返回小票
  - 遍历所有的 LockerSize 去存 Bag，直到存储成功
  - 所有 LockerSize 都存不了，抛异常，Cabinet 存不下了
- Stupid Assistant 取包
- Stupid Assistant 指定多个 Cabinet
  - 存包可尝试多个 Cabinet
  - 取包可尝试多个 Cabinet
- Lazy Assistant 以当前 Bag 尺寸开始，LockerSize 从小到大去尝试存储

- Skillful Assistant ？
- Manager ！
