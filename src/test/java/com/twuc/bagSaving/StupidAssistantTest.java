package com.twuc.bagSaving;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

class StupidAssistantTest extends BagSavingArgument {
    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_save_bag_by_stupid_assistant(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        StupidAssistant assistant = new StupidAssistant(cabinet);
        Ticket ticket = assistant.save(new Bag(bagSize));
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_bag_by_stupid_assistant(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        StupidAssistant assistant = new StupidAssistant(cabinet);
        Bag myBag = new Bag(bagSize);
        Ticket ticket = assistant.save(myBag);
        Bag bag = assistant.getBag(ticket);
        assertSame(myBag, bag);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_bag_by_other_stupid_assistant(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        StupidAssistant assistantFoo = new StupidAssistant(cabinet);
        StupidAssistant assistantBar = new StupidAssistant(cabinet);
        Bag myBag = new Bag(bagSize);
        Ticket ticket = assistantFoo.save(myBag);
        Bag bag = assistantBar.getBag(ticket);
        assertSame(myBag, bag);
    }
}
