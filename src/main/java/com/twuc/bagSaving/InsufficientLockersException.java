package com.twuc.bagSaving;

public class InsufficientLockersException extends RuntimeException {
    InsufficientLockersException(String message) {
        super(message);
    }
}
