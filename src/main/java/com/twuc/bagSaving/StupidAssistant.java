package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;

public class StupidAssistant {
    private List<Cabinet> cabinets = new ArrayList<>();

    public StupidAssistant(Cabinet cabinet) {
        this.cabinets.add(cabinet);
    }

    public StupidAssistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
    }

    public Ticket save(Bag bag) {
        Ticket ticket = null;
        for (Cabinet cabinet : cabinets) {
            for (LockerSize lockerSize : LockerSize.values()) {
                try {
                    ticket = cabinet.save(bag, lockerSize);
                    break;
                } catch (Exception e) {
                    // trying...
                }
            }
        }
        if (ticket == null) {
            throw new InsufficientLockersException("Insufficient empty lockers.");
        }
        return ticket;
    }

    public Bag getBag(Ticket ticket) {
        Bag bag = null;
        for (Cabinet cabinet : cabinets) {
            try {
                bag = cabinet.getBag(ticket);
                break;
            } catch (Exception e) {
                // trying...
            }
        }
        if (bag == null) {
            throw new IllegalArgumentException("Invalid ticket.");
        }
        return bag;
    }
}
